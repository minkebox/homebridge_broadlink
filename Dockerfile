FROM registry.gitlab.com/minkebox/homebridge_base

COPY root/ /

HEALTHCHECK --interval=60s --timeout=5s CMD /health.sh

RUN apk add python3 alpine-sdk; cd /app ; npm --save install homebridge-broadlink-rm-pro; apk del python3 alpine-sdk
